DROP DATABASE IF EXISTS small_business_db;

CREATE DATABASE IF NOT EXISTS small_business_db;

-- CREATE

CREATE TABLE usuario (
        usuario_id SERIAL,
        nome VARCHAR
);

CREATE TABLE empresa (
  empresa_id INTEGER,
  nome VARCHAR,
  fundacao TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE produto (
  produto_id INTEGER,
  nome VARCHAR,
  descricao VARCHAR
);

CREATE TABLE inventario (
  empresa_id INTEGER,
  produto_id INTEGER,
  quantidade
);

CREATE TABLE pedido (
  pedido_id INTEGER
);

-- INSERT

INSERT INTO empresa (
  nome, fundacao
) VALUES (
  'Bolo de Rolo Artesanal', '2011-08-10'
);

INSERT INTO produto (
  nome
) VALUES (
  'Bolo de Rolo',
  'Açúcar Refinado União',
  'Farinha de Trigo',
  'Ovos',
  'Margarina Primor',
  'Goiabada Anhembi',
  'Doce de Leite Itambé',
  'Chocolate Harald'
);

INSERT INTO inventario (
  empresa_id, produto_id, quantidade
) VALUES (
  1, 1, 10
);
