# Aula 08

**Modelagem lógica e física dos dados a persistir**

Objetivos:

- Revisar as regras de geração de bancos de dados relacionais com base no modelo conceitual de dados;
- Definir o modelo físico de dados, o que inclui a definição física dos SGBD e de sua forma de armazenamento;
- Aplicar o modelo físico de dados em seu projeto de TCC;
- Definir os arquivos necessários em aplicação mobile.
