# Aula 10

**Encerrando o seu projeto**

Objetivos:

- Revisar o conteúdo;
- Verificar as referências bibliográficas e a consistência entre os modelos correlatos;
- Definir as conclusões do seu projeto.
