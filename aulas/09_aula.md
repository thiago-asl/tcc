# Aula 9: Ambiente de desenvolvimento e operação do sistema

Objetivos:

- Ilustrar os conceitos e elementos do diagrama de Implantação;
- Aplicar a construção do diagrama de Implantação ao projeto de seu TCC;
- Definir e justificar a linguagem de programação e o SGBD a ser usados.
