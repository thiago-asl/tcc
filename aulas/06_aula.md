# Aula 06

**Modelo de classes de Projeto (_Projeto de algoritmos_)**

Objetivos:

- Definir os conceitos e elementos do diagrama de atividades;
- Avaliar métodos e classes para entender a necessidade do diagrama de atividades;
- Aplicar a construção do diagrama de atividades ao projeto de seu TCC.
