# Aula 01

**A revisão do Projeto de TCC e sua continuidade**

Objetivos:

- Descrever o processo de revisão do Projeto de TCC aprovado;
- Identificar, na estrutura, o conteúdo do TCC a ser completado nesta disciplina;
- Revisar as regras ABNT para formatação e conteúdo do seu TCC.

Definição das tecnologias:

- Clientes
- Produtos
- Pessoas
- Processos
- Informação

Explicando a Estrutura de Conteúdo de TCC

- Modelagem de Sistemas (orientação a objetos e diagramas UML)
- Modelagem de Dados (modelo conceitual de dados --- MER)
- Engenharia de Usabilidade (projeto de interfaces)
- Implementação de banco de dados (modelo relacional e linguagem SQL)
