# Aula 02

**Modelando as interações**

Objetivos:

- Identificar o uso dos diagramas de interação: sequência e comunicação;
- Reconhecer os elementos e a elaboração dos diagramas de sequência e
    comunicação;
- Aplicar os diagramas de interação em seu projeto de TCC.

> De forma geral, os diagramas de interação mostram como os objetos colaboram
> em determinados comportamentos
>
> O diagrama de sequência é mais adequado quando o foco é a temporalidade da
> interação, para mostrar a ordem em que as mensagens são trocadas
>
> O diagrama de comunicação, foca nas mensagens enviadas entre objetos que
> estão relacionados, sem se ater a ordem

![Workflow](./assets/aula-2-diagrama-1.png)

## Diagrama de Sequência

Componentes de um:

- Atores;
- Objetos que participam da interação;
- Mensagens trocadas (e seus parâmetros);
- Linha de vida e Caixa de ativação;
- Operadores de Controle estruturado;
- Foco de Controle;
- Gate.

### Atores

- Representam papéis que interagem com o sistema e seus objetos;
- Um ator está sempre fora do escopo do sistema modelado;
- Os atores são empregados para representar usuários humanos e outros
    elementos externos.

### Objetos

São os elementos que participam das interações no diagrama.

- Interagem por meio de mensagens;
- Colocados no nível superior do diagrama, ao longo do eixo `x`;
- Objeto que inicia a interação é colocado à esquerda;
- Objetos subordinados à direita

Um objeto é representado por um retângulo com seu nome.

### Linha de Vida

Elemento nomeado que representa um participante interno individual na interação.

A linha de vida de um objeto é uma linha tracejada vertical que representa o
período de tempo no qual um objeto existe.

### Foco de Controle

Período no qual um objeto está participando ativamente de um processo, ou seja,
o tempo necessário para que um objeto complete uma tarefa. Também chamado de
_Caixa de Ativação_.

Representado dentro da linha de vida de um objeto, como um retãngulo na
vertical.

> Os atores não precisam de caixas de ativação, pois são externos.

### Mensagens

Demonstram a ocorrência de eventos (chamadas de métodos) ou comunicação entre
objetos, sem chamar métodos.

Mostram as informações que são enviadas entre os objetos.

As mensagens são representadas por uma seta horizontal que vai de uma linha de
vida à outra, apontando para o destinatário da mensagem.

#### Tipos de Mensagens

Existem vários tipos de mensagens empregadas em diagramas de sequência, sendo
as mais comuns as seguintes:

- Mensagem Síncrona;
- Mensagem Assíncrona;
- Auto-mensagem;
- Mensagem de Resposta;
- Mensagem de Criação de Participante;
- Mensagem de Exclusão de Participante;
- Mensagem de Guarda.

##### Síncrona

Mensagem que espera por uma resposta antes que a interação possa prosseguir.
Remetente espera até que o receptor tenha terminado o processamento da mensagem.
Grande parte das mensagems é síncrona.

Usamos uma seta de ponta sólida para representar a mensagem síncrona.

##### Assíncrona

Mensagem que não espera por uma resposta do destinatário antes que a interação
possa prosseguir. A interação prossegue independente do destinatário processar
a mensagem ou não.

Usamos uma seta de ponta linear para representar a mensagem assíncrona.

##### Criação de Participante

Uma mensagem de criação permite instanciar um novo objeto no diagrama.
As vezes, é necessário que uma mensagem crie um novo objeto.

Representada por uma seta tracejada com o rótulo `<<create>>` em uma tag.

##### Exclusão de Participante

A mensagem de exclusão é empregada para eliminar um objeto. Assim, a memória é
desalocada pela destruição da ocorrência do objeto no sistema.

Representada por uma seta que termina com um __X__.

##### Auto-Mensagem

Uma auto-mensagem é empregada quando um objeto necessita realizar uma chamada
a si mesmo.

Representada com uma seta que sai e volta ao mesmo objeto.

Pode ser síncrona ou assíncrona.

##### Mensagem de Resposta

Uma mensagem de resposta (ou de retorno) identifica a resposta a uma mensagem
enviada para o objeto.

Pode retornar informações específicas ou apenas uma mensagem de êxito ou
falha na execução de um método.

Representadas por uma linha fina tracejada que aponta para o objeto que recebe
o resultado de retorno.

##### Mensagens de Guarda

As mensagens de guarda são usadas para modelar condições.

Úteis quando é necessário restringir o fluxo de uma mensagem de acordo com uma
condição.

Representadas por uma elipse ao redor da seta de mensagem, indicando a condição.

### Gate

Um Gate (Portão) é o final de uma mensagem, ou seja, um ponto de conexão entre
uma mensagem que esteja dentro de um fragmento de interação com outra que
esteja fora.

São representados como pontos de conexão de mensagem.

### Tempo de Vida de um Objeto

o tempo de vida de um objeto se refere ao tempo em que ele existe, independente
de estar realizando algum processo ou não. Quando um objeto não é mais
necessário no sistema, seu tempo de vida pode expirar, e o objeto pode ser
removido com uma mensagem de exclusão `destroy()`.

### Fragmentos Combinados e Controle Estruturado

Muitas vezes precisamos mostrar condicionais e loops, ou ainda a execução
concorrente de várias sequências.

Para isso usamos __Operadores de Controle Estruturado__ (ou Operadores de
Interação).

Um fragmento combinado define uma combinação de fragmentos de interação.
Definido por um operador de interação e operandos de interação.

Região retangular no diagrama, com uma tag que informa o tipo de operador.
Aplica-se às linhas de vida que atravessam seu corpo.

| Operador | Descrição |
| --- | --- |
| `opt` | Execução opcional (um ou nada) |
| `alt` | Execução condicional (um ou outro) |
| `par` | Execução paralela |
| `loop` | Execução de loop (iteração) |
| `strict` | Ordem Sequencial Estrita |
| `neg` | Interação Inválida ou Negação |
| `ref` | Interação com outro diagrama |
| `break` | Quebra de execução (exceção) |
