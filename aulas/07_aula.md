# Aula 07

**Componentes do sistema**

Objetivos:

- Conceituar elementos do diagrama de componentes;
- Definir a necessidade de componentes no seu projeto;
- Aplicar a construção do diagrama de componentes ao projeto de seu TCC.
