# Aula 03

**Projeto de interfaces**

Objetivos:

- Reconhecer a importância da interface e do modelo de interação do sistema;
- Construir o diagrama de estados de navegação;
- Definir o layout e os elementos de cada janela e de cada saída do sistema.
