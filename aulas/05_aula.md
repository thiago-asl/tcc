# Aula 05

**Projeto de algoritmos**

Objetivos:

- Explicar os conceitos, propriedades e técnicas para evoluir o diagrama conceitual de classes para diagrama de classes de projeto;
- Examinar conceitos de coesão e acoplamento, bem como padrões de projeto e outros elementos de reuso;
- Aplicar a construção do diagrama de classes de projeto em seu TCC a partir do diagrama conceitual de classes
