INSERT INTO empresa (
  nome, fundacao
) VALUES (
  'Bolo de Rolo Artesanal', '2011-08-10'
);

INSERT INTO produto (
  nome
) VALUES (
  'Bolo de Rolo',
  'Açúcar Refinado União',
  'Farinha de Trigo',
  'Ovos',
  'Margarina Primor',
  'Goiabada Anhembi',
  'Doce de Leite Itambé',
  'Chocolate Harald'
);

INSERT INTO inventario (
  empresa_id, produto_id, quantidade
) VALUES (
  1, 1, 10
);
