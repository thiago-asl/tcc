-- Tabela de Empresa
CREATE TABLE empresa (
  empresa_id INTEGER,
  nome VARCHAR,
  fundacao TIMESTAMP WITHOUT TIME ZONE
);

-- Tabela de Produto
CREATE TABLE produto (
  produto_id INTEGER,
  nome VARCHAR,
  descricao VARCHAR
);

-- Tabela de Inventário
CREATE TABLE inventario (
  empresa_id INTEGER,
  produto_id INTEGER,
  quantidade
);

CREATE TABLE pedido (
  pedido_id INTEGER
);
