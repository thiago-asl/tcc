# TCC em Sistemas de Informação

> [Regras da ABNT para TCC 2020: as principais normas](https://viacarreira.com/regras-da-abnt-para-tcc-conheca-principais-normas/)

## Estrutura do Trabalho

- 3.3. Solução Tecnológica
  - 3.3.1. Diagrama de Sequência (ou comunicação)
  - 3.3.2. Projeto de Interfaces
  - 3.3.3. Diagrama de Estados
  - 3.3.4. Diagrama de Atividades
  - 3.3.5. Diagrama de Componentes
  - 3.3.6. Modelo de classes de Projeto
  - 3.3.7. Modelo Físico de dados
    - 3.3.7.1. Projeto de Tabelas e Arquivos
    - 3.3.7.2. Scripts de geração do banco e suas tabelas
  - 3.3.8. Ambiente tecnológico do sistema
    - 3.3.8.1. Ambiente Físico (diagrama de implantação)
    - 3.3.8.2. Justificativa da escolha da linguagem de programação
    - 3.3.8.3. Justificativa da escolha do SGBD (Sistema Gerenciador de Banco de Dados)
- 4. Conclusões
  - 4.1. Reflexões e comparação entre objetivos iniciais x alcançados
  - 4.2. Vantagens e desvantagens do sistema
  - 4.3. Trabalhos futuros
- 5. Referências Bibliográficas

## Entregas

| Nome                 | Data       | Status |
| -------------------- | ---------- | ------ |
| Entrega Inicial      | 09/10/2020 | OK     |
| Entrega da Modelagem | 19/10/2020 |        |

- **Entrega Inicial**
  - Projeto de TCC
- **Entrega da Modelagem**
  - 3.3.1. Diagrama de Sequência (ou comunicação)
  - 3.3.2. Projeto de Interfaces
  - 3.3.3. Diagrama de Estados
  - 3.3.4. Diagrama de Atividades
  - 3.3.5. Diagrama de Componentes
  - 3.3.6. Modelo de classes de Projeto
